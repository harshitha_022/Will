import React,{Component} from 'react';
import Home from "./components/homepage/Home";
import About from "./components/aboutpage/About";
import Skills from "./components/aboutpage/Skills";
import Projects from "./components/projectpage/Projects";
import Contact from "./components/contactpage/Contact";
import ECskills from './components/aboutpage/ECskills';
import {BrowserRouter as Router,Switch,Route,Redirect,Routes} from "react-router-dom";
import Videomodal from './components/projectpage/Videomodal';


function App() {
  return (
    

      <Router>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/projects" element={<Projects />} />
          <Route path="/contact" element={<Contact />} />
          <Route path="/skills" element={<Skills />} />
          <Route path="/kmore" element={<ECskills />} />
        </Routes>
      </Router> 
    
  )
}

export default App;








// import './App.scss';
// import Title from './components/Title';

// class App extends Component{
//     render()
//     {
//         return(
//             <div className='App'>
//                 <div className='page'>
//                     <Title text="Harshitha Sivalingala"/>
//                 </div>
//             </div>
//         )
//     }
// }
// export default App;