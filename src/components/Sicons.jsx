import React from 'react'
import "./Sicons.scss";
import {
  SiLeetcode,
} from "react-icons/si";

const Sicons = () => {
  return (
  <div class= "Sicons">
    <a href="https://gitlab.com/harshitha_022"><i class="fa-brands fa-gitlab"></i></a>
    <a href="https://www.linkedin.com/in/harshithasivalingala/"><i class="fab fa-linkedin-in"></i></a>
    <a href="https://leetcode.com/harshitha_022/"><SiLeetcode/></a>
    <a href="#"><i class="fa-brands fa-github"></i></a>

 </div>
  )
}

export {Sicons};